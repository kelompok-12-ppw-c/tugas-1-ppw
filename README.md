# Kelompok 12 PPW C Tugas 1 & Tugas 2
Members: 
    - Farril Zavier Fernaldy : Pengerjaan Programs | Pengerjaan Login (Merah) dan Daftar Donasi (Hijau)
    - Romi Hadiyan Aji Witjaksono : Pengerjaan Homepage Signup | Pengerjaan Daftar List Program/Donasi ke Suatu Program (Biru)
    - Yafonia Kristiani Martina Napadot Hutabarat : Pengerjaan News | Pengerjaan About/Testimoni (Kuning)
    
Status Pipelines:
[![Pipeline](https://gitlab.com/kelompok-12-ppw-c/tugas-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/kelompok-12-ppw-c/kelompok-12-ppw-c-tugas-1/commits/master)
[![Coverage](https://gitlab.com/kelompok-12-ppw-c/tugas-1-ppw/badges/master/coverage.svg)](https://gitlab.com/kelompok-12-ppw-c/kelompok-12-ppw-c-tugas-1/commits/master)



Link Heroku:
https://tugas1-ppw-c12.herokuapp.com/

