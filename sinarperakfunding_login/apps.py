from django.apps import AppConfig


class SinarperakfundingLoginConfig(AppConfig):
    name = 'sinarperakfunding_login'
