from django.urls import path
from .views import login, logout, check_user_authenticated

urlpatterns = [
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('check_user_authenticated/', check_user_authenticated, name='check_user_authenticated'),
]
