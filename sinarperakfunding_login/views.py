from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from google.oauth2 import id_token
from google.auth.transport import requests


# Create your views here.
response = {}

def login(request):
    response['navItem'] = 'login'
    request.session['navItem'] = response['navItem']
    if request.method == 'POST':
        try:
            token = request.POST['token_id']
            info_id = id_token.verify_oauth2_token(token, requests.Request(), '464361038996-o21jn0q1ch6msar62p17qr58mehlriol.apps.googleusercontent.com')
            if info_id['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            user_id = info_id['sub']
            name = info_id['name']
            email = info_id['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            return JsonResponse({'status': '0', 'url': reverse('programs')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html', response)

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))

def check_user_authenticated(request):
    if 'user_id' in request.session.keys():
        return JsonResponse({'is_authenticated': True})
    else:
        return JsonResponse({'is_authenticated': False})
