from django.test import TestCase, Client
from django.urls import resolve
from .views import login, logout, check_user_authenticated


# Create your tests here.

class UnitTest(TestCase):
    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_login_with_wrong_token(self):
        response = Client().post('/login/', {'token_id': 'some_id'}).json()
        self.assertEqual(response['status'], '1')

    def test_logout_url_redirects(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_logout_using_logout_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout)

    def test_func_check_user_authenticated_while_not_logged_in(self):
        response = Client().get('/check_user_authenticated/').json()
        self.assertFalse(response['is_authenticated'])
