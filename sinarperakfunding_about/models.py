from django.db import models
import django.utils.datetime_safe

# Create your models here.

class Testimony(models.Model):
    testimony = models.CharField(max_length=500)
    name = models.CharField(max_length = 300, blank=True)
