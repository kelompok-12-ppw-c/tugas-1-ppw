from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import Testi_Form
from .models import Testimony
import json 
import requests
import re
from django.forms.models import model_to_dict
from django.contrib.auth.decorators import login_required 


def testimony_objects(request):
    response = {}
    if 'user_id' in request.session.keys():
        response['is_authenticated'] = True
    else:
        response['is_authenticated'] = False
    testimony_list = Testimony.objects.all()
    form = Testi_Form()
    return render(request, "about.html", {'form' : form, 'testimonies' : testimony_list, 'is_authenticated': response['is_authenticated']})

@csrf_exempt
def post(request):
    response = {}

    if (request.method == 'POST'):
        testimony = request.POST.get('post')
        print(testimony)
        response['name'] = request.session['name']
        name = response['name']
        # response['testimony'] = testimony
        newtestimony = Testimony(name = name, testimony = testimony)
        newtestimony.save()
        response['testimony'] = model_to_dict(newtestimony)
        return HttpResponse(json.dumps(response), content_type="application/json")

    else:
        return HttpResponse(json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json")


# Create your views here.
