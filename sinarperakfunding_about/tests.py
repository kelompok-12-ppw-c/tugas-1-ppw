from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import post
from .views import testimony_objects
from .models import Testimony
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time

class AboutUnitTest(TestCase):

    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)

    def test_post_url_is_exist(self):
        response = Client().get('/post/')
        self.assertEqual(response.status_code,200)

    def test_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')
    
    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, testimony_objects)
    
    def test_about_using_about_func(self):
        found = resolve('/post/')
        self.assertEqual(found.func, post)

    def test_model_can_create_new_testimony(self):
        new_testimony = Testimony.objects.create(testimony = 'create new testimony')
        counting_all_available_testimony = Testimony.objects.all().count()
        self.assertEqual(counting_all_available_testimony, 1)

    
    
# class AboutFunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(AboutFunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(AboutFunctionalTest, self).tearDown()


#     def test_title(self):
#         # self.browser.get('http://tugas1-ppw-c12.herokuapp.com/about/')
#         self.browser.get('http://localhost:8000/about/')
#         time.sleep(5)
#         self.assertIn("SinarPerak", self.browser.title)
        

#     def test_header(self):
#         self.browser.get('http://localhost:8000/about/')
# #         self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         header_text = self.browser.find_element_by_tag_name('h1').text
#         time.sleep(5)
#         self.assertIn("About Us", header_text)
    
#     def test_header_with_css_property(self):
#         # self.browser.get('http://ppw-c-yafonia.herokuapp.com/Hello/')
#         self.browser.get('http://localhost:8000/about/')
#         header_text = self.browser.find_element_by_tag_name('h1').value_of_css_property('text-align')
#         time.sleep(5)
#         self.assertIn('center', header_text)
    




# Create your tests here.
