from django.apps import AppConfig


class SinarperakfundingAboutConfig(AppConfig):
    name = 'sinarperakfunding_about'
