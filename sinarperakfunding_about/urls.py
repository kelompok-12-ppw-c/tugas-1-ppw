from django.urls import path
from .views import post
from .views import testimony_objects


urlpatterns = [
    path('about/', testimony_objects, name = 'testimony'),
    path('post/', post, name = 'testimonypost')
]