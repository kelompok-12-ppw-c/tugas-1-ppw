from django import forms
from django.forms import ModelForm
from .models import Testimony

class Testi_Form(forms.ModelForm):
    class Meta:
        model = Testimony
        fields = ['testimony']
        widgets = {
            'testimony': forms.Textarea(
                attrs={'class': 'form-control', 'id': 'post-text',  'placeholder': 'Write your testimony here...' , 'maxlength': 500}),
        }

