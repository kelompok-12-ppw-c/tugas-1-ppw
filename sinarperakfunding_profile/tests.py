from django.test import TestCase, Client
from django.urls import resolve
from .views import profile


# Create your tests here.


class UnitTest(TestCase):

    def test_profile_url_exists(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 302)

    def test_profile_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
