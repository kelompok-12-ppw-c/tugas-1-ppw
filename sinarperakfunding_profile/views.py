from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from sinarperakfunding_programs.models import Donation, Program

# Create your views here.
response = {}

def profile(request):
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login'))
    response['navItem'] = 'profile'
    request.session['navItem'] = response['navItem']
    if 'user_id' in request.session.keys():
        programs = []
        donations = Donation.objects.all().filter(email=request.session['email']).values()
        for donation in donations:
            programs.append(Program.objects.get(id=donation['program_id']))
        response['donations'] = zip(list(donations), programs)
    return render(request, 'profile.html', response)
