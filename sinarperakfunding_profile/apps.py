from django.apps import AppConfig


class SinarperakfundingProfileConfig(AppConfig):
    name = 'sinarperakfunding_profile'
