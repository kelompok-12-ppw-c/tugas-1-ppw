"""kelompok2ppwc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import sinarperakfunding_home.urls
import sinarperakfunding_news.urls
import sinarperakfunding_programs.urls
import sinarperakfunding_login.urls
import sinarperakfunding_about.urls
import sinarperakfunding_profile.urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(sinarperakfunding_news.urls)),
    path('', include(sinarperakfunding_programs.urls)),
    path('', include(sinarperakfunding_home.urls)),
    path('', include(sinarperakfunding_login.urls)),
    path('', include(sinarperakfunding_about.urls)),
    path('', include(sinarperakfunding_profile.urls)),
]
