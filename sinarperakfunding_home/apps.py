from django.apps import AppConfig


class SinarperakfundingHomeConfig(AppConfig):
    name = 'sinarperakfunding_home'
