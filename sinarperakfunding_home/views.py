from django.shortcuts import render

# Create your views here.
def home(request):
    response = {'navItem': 'home'}
    return render(request, 'home.html', response)
