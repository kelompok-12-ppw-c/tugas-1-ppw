$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
    $.ajax({
        url: "/check_user_authenticated/",
        success: function(result) {
            if (result.is_authenticated) {
                console.log('User is logged in');
                $('#login-logout-btn').replaceWith('<a id="login-logout-btn" class="dropdown-item" href="/logout/" onclick="signOut();">Logout</a>');
                $('#profile-btn').replaceWith('<a id="profile-btn" class="dropdown-item" href="/profile/">Profile</a>');
            } else {
                console.log('User is logged out');
                var navStatus = '';
                if (result.navItem === 'login') {
                    navStatus = 'active'
                }
                $('.dropdown-toggle').replaceWith('<a class="text-light nav-link dropdown-toggle btn btn-danger" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account</a>');
                $('#login-logout-btn').replaceWith('<a id="login-logout-btn" class="dropdown-item ' + navStatus + ' " href="/login/">Login</a>');
                $('#profile-btn').replaceWith('<a id="profile-btn"></a>');
            }
        },
    });
});

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var token_id = googleUser.getAuthResponse().id_token;
    sendToken(token_id);
    console.log('Logged in');
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {token_id: token},
        success: function(result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                console.log("Successfully logged in");
                window.location.replace(result.url);
            } else {
                console.log("Something went wrong.");
            }
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}