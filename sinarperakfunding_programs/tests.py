from importlib import import_module
from django.conf import settings
from django.test import TestCase, Client
from django.urls import resolve
from .forms import Donation_Form
from .models import Donation, Program
from .views import programs, program_details, thankyou


# Create your tests here.


class UnitTest(TestCase):

    def setUp(self):
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

    def test_programs_url_exists(self):
        response = Client().get('/programs/')
        self.assertEqual(response.status_code, 200)

    def test_programs_uses_programs_func(self):
        found = resolve('/programs/')
        self.assertEqual(found.func, programs)

    def test_programs_has_title(self):
        response = Client().get('/programs/')
        title = response.content.decode('utf-8')
        self.assertIn("<h1 class=\"container my-4\">PROGRAMS</h1>", title)

    def test_model_can_create_new_donation(self):
        donation = Donation.objects.create(fullname='anon', email='anon@gmail.com', amount=50000)
        total_donations = Donation.objects.all().count()
        self.assertEqual(total_donations, 1)

    def test_form_valid_for_blank_entries(self):
        form = Donation_Form(data={'amount': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['amount'], ["This field is required."])

    def test_model_can_create_new_program(self):
        program = Program.objects.create(program_title='program', program_content='content', program_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        total_programs = Program.objects.all().count()
        self.assertEqual(total_programs, 1)

    def test_program_details_url_exists(self):
        program = Program.objects.create(program_title='program', program_content='content', program_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        response = Client().get('/programs/program_details/' + str(program.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_donation_uses_program_details_func(self):
        program = Program.objects.create(program_title='program', program_content='content', program_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        found = resolve('/programs/program_details/' + str(program.id) + '/')
        self.assertEqual(found.func, program_details)

    # def test_donation_post_success_and_redirect(self):
    #     session = self.client.session
    #     session['name'] = 'Some Name'
    #     session['email'] = 'somename@gmail.com'
    #     program = Program.objects.create(program_title='program', program_content='content', program_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
    #     test = 50000
    #     response_post = Client().post('/programs/program_details/' + str(program.id) + '/', {'amount': test})
    #     self.assertEqual(response_post.status_code, 302)

    def test_thankyou_url_exists(self):
        response = Client().get('/programs/thankyou/')
        self.assertEqual(response.status_code, 200)

    # def test_user_anonymous(self):
    #     session = self.client.session
    #     session['name'] = 'Some Name'
    #     session['email'] = 'Anonymous@anon.com'
    #     program = Program.objects.create(program_title='program', program_content='content', program_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
    #     test_1 = 50000
    #     test_2 = True
    #     response_post = Client().post('/programs/program_details/' + str(program.id) + '/', {'amount': test_1, 'anonymous': test_2})
    #     found = resolve('/programs/thankyou/')
    #     self.assertEqual(response_post.status_code, 302)
    #     self.assertTrue(found.func, thankyou)
