from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Donation_Form
from .models import Donation, Program
from django.contrib.auth.decorators import login_required 

# Create your views here.
response = {}

def programs(request):
    if 'user_id' in request.session.keys():
        response['is_authenticated'] = True
    else:
        response['is_authenticated'] = False
    response['navItem'] = 'programs'
    program_objects = Program.objects.all()
    response['program_objects'] = program_objects
    response['total_programs'] = Program.objects.all().count()
    return render(request, 'programs.html', response)

#@login_required(login_url="/login/")
def program_details(request, program_id):
    response['donations'] = Donation.objects.all().filter(program_id=program_id)
    response['program'] = Program.objects.get(id=program_id)
    if request.method == 'POST':
        form = Donation_Form(request.POST)
        if form.is_valid():
            if 'anonymous' in request.POST:
                response['fullname'] = 'Anonymous'
            else:
                response['fullname'] = request.session['name']
            response['email'] = request.session['email']
            response['amount'] = request.POST['amount']
            response['program_id'] = program_id
            donation = Donation(fullname=response['fullname'], email=response['email'], amount=response['amount'], program_id=response['program_id'])
            donation.save()
            return HttpResponseRedirect('/programs/thankyou/')
    else:
        form = Donation_Form()
    response['form'] = form

    if 'user_id' in request.session.keys(): 
        return render(request, 'program_details.html', response)
    else :
        return render(request, 'login.html', response)

def thankyou(request):
    response['navItem'] = 'programs'
    return render(request, 'thankyou.html', response)
