from django import forms


class Donation_Form(forms.Form):
    amount = forms.CharField(label='Amount', required=True, widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Donation Amount'}))
    anonymous = forms.CharField(label='Anonymous?', required=False, widget=forms.CheckboxInput())
