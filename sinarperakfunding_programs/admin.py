from django.contrib import admin
from .models import Donation, Program

# Register your models here.
admin.site.register(Program)
admin.site.register(Donation)
