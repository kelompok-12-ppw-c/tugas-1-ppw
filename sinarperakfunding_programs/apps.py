from django.apps import AppConfig


class SinarperakfundingProgramsConfig(AppConfig):
    name = 'sinarperakfunding_programs'
