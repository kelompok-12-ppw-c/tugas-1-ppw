from django.urls import path
from .views import programs, program_details, thankyou

urlpatterns = [
    path('programs/thankyou/', thankyou, name='thankyou'),
    path('programs/program_details/<int:program_id>/', program_details, name='program_details'),
    path('programs/', programs, name='programs')
]
