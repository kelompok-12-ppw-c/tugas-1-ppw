from django.db import models

# Create your models here.
class Program(models.Model):
    program_title = models.CharField(max_length=30)
    program_content = models.CharField(max_length=100)
    program_image = models.CharField(max_length=100, unique=True)


class Donation(models.Model):
    fullname = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    amount = models.CharField(max_length=50)
    program_id = models.IntegerField(default=0)
