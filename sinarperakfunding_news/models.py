from django.db import models


class News(models.Model):
    # news_title = models.TextField(max_length = 30, default = "news_title")
    # news_content = models.TextField(max_length = 30, default = "news_content")
    # news_image = models.CharField(max_length=30, default = "https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg")
    
    news_title = models.CharField(max_length = 30)
    news_content = models.CharField(max_length = 3000)
    news_image = models.CharField(max_length = 100, unique=True)
    
# Create your models here.
