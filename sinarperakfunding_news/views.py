from django.shortcuts import render
from django.http import HttpResponse
from .models import News

def news(request):
    # response = {'judul' : News.news_title, 'isi' : News.news_content, 'foto' : News.news_image}
    news_objects = News.objects.all()
    response = {'news_objects' : news_objects}
    return render(request, 'news.html', response)

def newsdetails(request,idnews):
    # response = {'judul' : News.news_title, 'isi' : News.news_content, 'foto' : News.news_image}
    news_id = News.objects.get(id=idnews)
    response = {'news_id' : news_id}
    return render(request,'newsdetails.html', response)
# Create your views here.

