from django.urls import path
from .views import news
from .views import newsdetails

urlpatterns = [
    path('news/newsdetails/<int:idnews>/', newsdetails, name='newsdetails'),
    path('news/', news, name = 'news'),
]