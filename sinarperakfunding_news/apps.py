from django.apps import AppConfig


class SinarperakfundingNewsConfig(AppConfig):
    name = 'sinarperakfunding_news'
