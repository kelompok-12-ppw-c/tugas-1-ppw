from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import News
from .views import news
from .views import newsdetails

# Create your tests here.
class SinarPerakFundingTests(TestCase):

    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)
    
    def test_newsdetails_url_is_exist(self):
        news = News.objects.create(news_title='test', news_content='test', news_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        response = Client().get('/news/newsdetails/' + str(news.id) + '/')
        self.assertEqual(response.status_code,200)
        
    def test_tugas_1_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')
    
    def test_tugas_1_newsdetails_template(self):
        news = News.objects.create(news_title='test', news_content='test', news_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        response = Client().get('/news/newsdetails/' + str(news.id) + '/')
        self.assertTemplateUsed(response, 'newsdetails.html')
    
    def test_using_news_func_news(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news)

    def test_using_news_func_newsdetails(self):
        news = News.objects.create(news_title='test', news_content='test', news_image='https://66.media.tumblr.com/080bc28c861c34bc86df09cf8f3e070d/tumblr_pgprimurr11wct16uo1_540.jpg')
        found = resolve('/news/newsdetails/' + str(news.id) + '/')
        self.assertEqual(found.func, newsdetails)
    
    def test_model_can_create_new_news(self):
        new_news = News.objects.create()
        all_news = News.objects.all().count()
        self.assertEqual(all_news, 1)
    

    

    
# Create your tests here.
